﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class Bankszamla
    {
        public string Tulajdonos { get; private set; }
        int egyenleg;
        public bool Aktiv { get; set; }

        public Bankszamla(string tulajdonos, int egyenleg)
        {
            Tulajdonos = tulajdonos;
            this.egyenleg = egyenleg;
            Aktiv = true;
        }

        public void PenzBefizetes(int osszeg)
        {
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }

            egyenleg += osszeg;
        }

        public void PenzFelvetel(int osszeg)
        {
            if (osszeg > this.egyenleg)
            {
                int hianyzik = osszeg - this.egyenleg;
                throw new PenzFelvetelException(hianyzik, this);
            }
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }

            egyenleg -= osszeg;
        }
    }
}
