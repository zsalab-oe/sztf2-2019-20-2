﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class PenzFelvetelException : BankszamlaException
    {
        public int HianyzoOsszeg { get; }

        public PenzFelvetelException(int HianyzoOsszeg, Bankszamla Szamla) : base(Szamla)
        {
            this.HianyzoOsszeg = HianyzoOsszeg;
        }
    }
}
