﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Generikus_Lancolt_Lista
{
    class Szemely
    {
        string nev;
        int eletkor;

        public Szemely(string nev, int eletkor)
        {
            this.nev = nev;
            this.eletkor = eletkor;
        }

        public override string ToString()
        {
            return $"{nev} ({eletkor})";
        }
    }
}
