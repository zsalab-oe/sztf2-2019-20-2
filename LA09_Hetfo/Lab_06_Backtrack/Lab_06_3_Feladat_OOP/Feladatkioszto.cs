﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    // ==============================================================================================================
    //
    // Leszármazott osztály, mely a feladatnak megfelelően megvalósítja az Ft és Fk függvényeket.
    //
    // ==============================================================================================================
    class Feladatkioszto : BacktrackBase
    {
        public Feladatkioszto(int n, Dolgozo[,] r, int[] m) : base(n, r, m)
        {
        }

        protected override bool Ft(int szint, Dolgozo dolgozo)
        {
            return true;
        }

        protected override bool Fk(int szint, Dolgozo dolgozo, Dolgozo[] E)
        {
            int j = 0;
            while (j < szint && E[j].Nev != dolgozo.Nev)
                j++;
            return j >= szint;
        }
    }
}
