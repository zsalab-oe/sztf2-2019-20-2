﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Graf_Demo
{
    class GrafSzomszedsagiLista : Graf
    {
        List<int>[] L; // szomszédsági listák tömbje
        public GrafSzomszedsagiLista(int n) : base(n)
        {
            L = new List<int>[n]; // Tömb inicializálása.

            // Üres listák inicializálása. Ez se maradjon le! :)
            for (int i = 0; i < n; i++)
            {
                L[i] = new List<int>();
            }
        }

        public override List<int> Csucsok()
        {
            // ebben a példában csak egy egyszerű, számokat tartalmazó listára van szükségünk
            List<int> l = new List<int>();
            for (int i = 0; i < N; i++)
            {
                l.Add(i);
            }
            return l;
        }

        public override void ElFelvetel(int honnan, int hova)
        {
            // mivel irányítatlan a gráf, ezért mind a két "oldalról" jelezni kell a kapcsolatot
            L[honnan].Add(hova);
            L[hova].Add(honnan);
        }

        public override bool VezetEl(int honnan, int hova)
        {
            return L[honnan].Contains(hova);
        }
    }
}
