﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Graf_Demo
{
    abstract class Graf
    {
        public delegate void Muvelet(int tartalom);

        protected int N; // csúcsok száma

        protected Graf(int n)
        {
            N = n;
        }

        public abstract List<int> Csucsok();
        public abstract void ElFelvetel(int honnan, int hova);
        public abstract bool VezetEl(int honnan, int hova);
        public List<int> Szomszedok(int csucs)
        {
            List<int> l = new List<int>();
            foreach (int i in Csucsok())
            {
                if (VezetEl(csucs, i))
                {
                    l.Add(i);
                }
            }
            return l;
        }


        // List: .Net lista adatszerkezet
        // Queue: .Net sor adatszerkezet
        //Enqueue(elem): elem berakása
        //Dequeue(): legkorábban berakott elem kivétele és törlése
        public void SzelessegiBejaras(int start, Muvelet metodus)
        {
            Muvelet _metodus = metodus;

            Queue<int> S = new Queue<int>();            // S: elért, de még feldolgozatlan csúcsok
            List<int> F = new List<int>();              // F: elért (azaz feldolgozott vagy feldolgozásra váró) csúcsok halmaza*
            S.Enqueue(start);                           // a kiinduló csúcsot berakjuk a sorba
            F.Add(start);                               // és a halmazba
            while (S.Count != 0)                        // addig fut a ciklus, amíg van elem a sorban
            {
                int k = S.Dequeue();                    // kivesszük az elemet
                _metodus?.Invoke(k);                    // feldolgozzuk
                foreach (int x in Szomszedok(k))        // megvizsgáljuk az aktuális csúcs szomszédait
                {
                    if (!F.Contains(x))                 // eldöntjük, hogy x benne van-e F-ben
                    {                                   // ha nincs, az azt jelenti x-et most értük el először
                        S.Enqueue(x);                   // ezért berakjuk a sorba
                        F.Add(x);                       // és a halmazba
                    }
                }
            }
        }                                               // *halmaz: egy csúcs csak egyszer szerepelhet benne


        public void MelysegiBejaras(int start, Muvelet metodus)
        {
            Muvelet _metodus = metodus;
            List<int> F = new List<int>();
            MelysegiBejarasRek(start, F, _metodus);
        }

        void MelysegiBejarasRek(int k, List<int> F, Muvelet metodus)
        {
            F.Add(k);                                   // k: éppen feldolgozandó csúcs
            metodus?.Invoke(k);                         // F: feldolgozott elemek halmaza
            foreach (int x in Szomszedok(k))            // megvizsgáljuk az aktuális csúcs szomszédait
            {
                if (!F.Contains(x))                     // eldöntjük, hogy x benne van-e F-ben
                {                                       // ha nincs, az azt jelenti x-et még nem dolgoztuk fel
                    MelysegiBejarasRek(x, F, metodus);     // ezért rekurzív módon újrahívja önmagát az eljárás
                }
            }
        }
    }
}
