﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Ismetles
{
    class Program
    {
        static void Main(string[] args)
        {
            Ketrec[] ketrecek = new Ketrec[4];

            // "összecsukható" kódrészlet: #region ÉS #endregion
            #region Ketrecek feltöltése

            ketrecek[0] = new Ketrec(5);
            ketrecek[0].Felvetel(new Allat("Kaller", true, 5, AllatFaj.Kutya));
            ketrecek[0].Felvetel(new Allat("Killer", true, 3, AllatFaj.Kutya));
            ketrecek[0].Felvetel(new Allat("Kajás", true, 40, AllatFaj.Kutya));
            ketrecek[0].Felvetel(new Allat("Mici", false, 320, AllatFaj.Panda));
            ketrecek[0].Felvetel(new Allat("Kormos", true, 20, AllatFaj.Kutya));

            ketrecek[1] = new Ketrec(3);
            ketrecek[1].Felvetel(new Allat("Nándi", true, 4, AllatFaj.Nyul));
            ketrecek[1].Felvetel(new Allat("Marcsi", false, 320, AllatFaj.Panda));
            ketrecek[1].Felvetel(new Allat("Nóri", true, 4, AllatFaj.Nyul));

            ketrecek[2] = new Ketrec(5);
            ketrecek[2].Felvetel(new Allat("Karesz", true, 15, AllatFaj.Kutya));
            ketrecek[2].Felvetel(new Allat("Kati", false, 13, AllatFaj.Kutya));
            ketrecek[2].Felvetel(new Allat("Kolbász", true, 40, AllatFaj.Kutya));

            ketrecek[3] = new Ketrec(5);
            ketrecek[3].Felvetel(new Allat("Krumpli", true, 10, AllatFaj.Kutya));
            ketrecek[3].Felvetel(new Allat("Norbi", true, 5, AllatFaj.Nyul));
            ketrecek[3].Felvetel(new Allat("Kicsi", false, 10, AllatFaj.Kutya));
            ketrecek[3].Felvetel(new Allat("Nyami", false, 12, AllatFaj.Nyul));
            ketrecek[3].Felvetel(new Allat("Nindzsa", true, 2, AllatFaj.Nyul));

            #endregion

            foreach (var item in ketrecek)
            {
                Console.WriteLine(item.AzonosFajEsEllenkezoNemVanE());
            }

            Console.ReadLine();
        }

        static Ketrec LegtobbFaj(Ketrec[] A, AllatFaj faj)
        {
            int maxIdx = 0;
            int maxErt = A[0].FajDarab(faj);
            for (int i = 1; i < A.Length; i++)
            {
                int aktErt = A[i].FajDarab(faj);
                if (aktErt > maxErt)
                {
                    maxErt = aktErt;
                    maxIdx = i;
                }
            }
            Ketrec ket = A[maxIdx];
            return ket;
        }
    }
}
