﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Ismetles
{
    class Ketrec
    {
        Allat[] allatok;
        int letszam;

        public Ketrec(int meret)
        {
            this.allatok = new Allat[meret];
            letszam = 0;
        }

        public bool Felvetel(Allat uj)
        {
            if (letszam < allatok.Length)
            {
                allatok[letszam] = uj;
                letszam++;
                return true;
            }
            return false;
        }

        public bool Torol(Allat torlendo)
        {
            int j = 0;
            while (j < letszam && allatok[j] != torlendo)
                j++;
            if (j < letszam)
            {
                letszam--;
                while (j < letszam)
                {
                    allatok[j] = allatok[j + 1];
                    j++;
                }
                allatok[j] = null;
                return true;
            }
            return false;
        }

        public int FajDarab(AllatFaj faj)
        {
            int db = 0;
            for (int i = 0; i < letszam; i++)
            {
                if (allatok[i].Faj == faj)
                    db++;
            }
            return db;
        }

        bool FajEsNemVanE(AllatFaj faj, bool himnemu)
        {
            int j = 0;
            while (j < letszam && !(allatok[j].Faj == faj && allatok[j].Himnemu == himnemu))
            {
                j++;
            }
            return j < letszam;
        }

        Allat[] FajAllatok(AllatFaj faj)
        {
            int db = FajDarab(faj);
            Allat[] kimenet = new Allat[db];
            int j = 0, k = 0;
            while (k < kimenet.Length)
            {
                if (allatok[j].Faj == faj)
                {
                    kimenet[k] = allatok[j];
                    k++;
                }
                j++;
            }
            return kimenet;
        }

        public bool AzonosFajEsEllenkezoNemVanE()
        {
            int akt = 0;
            bool van = false;
            while (akt < letszam - 1 && !van)
            {
                int j = akt + 1;
                while (j < letszam && !(allatok[akt].Faj == allatok[j].Faj && allatok[akt].Himnemu != allatok[j].Himnemu))
                {
                    j++;
                }
                if (j < letszam)
                    van = true;
                else
                    akt++;
            }
            return van;
        }

    }
}
