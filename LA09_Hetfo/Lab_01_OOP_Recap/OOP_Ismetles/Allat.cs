﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Ismetles
{
    enum AllatFaj { Kutya, Panda, Nyul }

    class Allat
    {
        // auto-property generálás:
        // propg + Tab + Tab

        private string nev;
        public string Nev { get { return nev; } }
        public bool Himnemu { get; private set; }
        public int Suly { get; private set; }
        public AllatFaj Faj { get; private set; }

        // konstruktor generálás:
        // ctor + Tab + Tab
        // Ctrl + .
        public Allat(string nev, bool himnemu, int suly, AllatFaj faj)
        {
            this.nev = nev;
            Himnemu = himnemu;
            Suly = suly;
            Faj = faj;
        }
    }
}
