﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_IComparable_Teszt
{
    class Program
    {
        static void Main(string[] args)
        {
            Gyumolcs[] gyumik = new Gyumolcs[]
                {
                    new Gyumolcs("körte", 399),
                    new Gyumolcs("szilva", 699),
                    new Gyumolcs("BIOalma", 1499),
                    new Gyumolcs("dinnye", 129)
                };

            foreach (Gyumolcs gy in gyumik)
                Console.WriteLine(gy.ToString());

            // Miért jó a CompareTo?
            // => Beépített függvények is ezt használják, pl.: Array.Sort()

            Array.Sort(gyumik);

            Console.WriteLine("===== Rendezve: =====");
            foreach (Gyumolcs gy in gyumik)
                Console.WriteLine(gy);

            Console.ReadLine();
        }
    }
}
