﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_IComparable_Teszt
{
    class Gyumolcs : IComparable
    {
        string nev;
        int ar;

        public Gyumolcs(string nev, int ar)
        {
            this.nev = nev;
            this.ar = ar;
        }

        // IComparable interfész miatt kell
        public int CompareTo(object obj)
        {
            // CompareTo-n belül ÉN tudom megmondani, hogy MI ALAPJÁN legyen az összehasonlítás!

            // visszatérési érték
            //   - kisebb mint 0: (a Gyumolcs objektumokat rendezve) a 'this' példány megelőzi a paraméterként átadottat a sorrendben
            //   - 0: a példány és a paraméterként átadott azonos helyen szerepelnek a sorrendben
            //   - nagyobb mint 0: a paraméterként átadott előzi meg a példányt a sorrendben

            Gyumolcs masikGyumi = (Gyumolcs)obj;// as Gyumolcs;

            if (this.ar < masikGyumi.ar)
                return -1;
            else if (this.ar > masikGyumi.ar)
                return 1;
            else
                return 0;

            //return this.ar - masikGyumi.ar;
        }

        public override string ToString()
        {
            return $"{nev}\t{ar}Ft";
        }
    }
}
