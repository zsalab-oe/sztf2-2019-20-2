﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Interface_Futar
{
    class Program
    {
        static void Main(string[] args)
        {
            // Interfész használata objektumokat összekötő "kapocsként"
            // => úgy cserélhetjük le a futárcéget (egy jobbra), hogy a Webshop osztályt nem kell hozzá módosítanunk !!!


            //ChinaPost futar = new ChinaPost();
            //IFutarceg futarRef2 = futar;
            //Webshop aliexpress = new Webshop(futar);

            DHLExpress dhlFutar = new DHLExpress();
            Webshop aliexpress = new Webshop(dhlFutar);

            aliexpress.Rendeles();

            Console.ReadLine();
        }
    }
}
