﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Interface_Futar
{
    class DHLExpress : IFutarceg
    {
        public string Szallit()
        {
            Console.WriteLine("[DHL] - Express szállítás! Csomagja 5-7 nap múlva érkezik.");
            string TN = TNGeneral().ToString() + "DHL";
            return TN;
        }

        int TNGeneral()
        {
            Random rnd = new Random();
            return rnd.Next(1000000, 10000000);
        }
    }
}
