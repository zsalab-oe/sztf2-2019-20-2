﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Interface_Alapok
{
    class Kutya : IKedvencHaziallat
    {
        // propfull + Tab + Tab
        private string nev;
        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }


        public void Eszik()
        {
            Console.WriteLine($"{nev}: Ettem.");
        }

        public void Setal(string hol)
        {
            Console.WriteLine($"{nev}: Sétáltam a(z) {hol}-ban/ben.");
        }

        public void Ugat()
        {
            Console.WriteLine($"{Nev}: Vau.");
        }
    }
}
