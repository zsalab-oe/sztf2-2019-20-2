﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Interface_Alapok
{
    class Papagaj : IHaziallat
    {
        private string nev;

        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }

        public void Eszik()
        {
            Console.WriteLine($"{nev}: Ettem.");
        }
    }
}
