﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Interface_Alapok
{
    class Kutya : IKedvencHaziallat
    {
        public string Nev { get; set; }

        public void Eszik()
        {
            Console.WriteLine($"{Nev}: Ettem.");
        }

        public void Setal(string hol)
        {
            Console.WriteLine($"{Nev}: Sétáltam a {hol}ban.");
        }

        public void Ugat()
        {
            Console.WriteLine($"{Nev}: Vau.");
        }
    }
}
