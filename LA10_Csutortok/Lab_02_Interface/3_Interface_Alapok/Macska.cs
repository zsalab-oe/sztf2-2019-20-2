﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Interface_Alapok
{
    class Macska : IHaziallat, IVadasz // többszörös öröklődés (-szerű!) is megvalósítható (egy osztály több interfészt is megvalósíthat)
    {
        // IHaziallat interfész miatt kellenek
        public string Nev { get; set; }

        public void Eszik()
        {
            Console.WriteLine($"{Nev}: Ettem.");
        }


        // IVadasz interfész miatt kell
        public void Vadaszik()
        {
            Console.WriteLine($"{Nev}: Megettem a papagájt.");
        }
    }
}
