﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Interface_Futar
{
    class Program
    {
        static void Main(string[] args)
        {
            // Interfész használata objektumokat összekötő "kapocsként"
            // => úgy cserélhetjük le a futárcéget (egy jobbra), hogy a Webshop osztályt nem kell hozzá módosítanunk !!!

            //IFutarceg futar = new ChinaPost();
            //Webshop aliexpress = new Webshop(futar);
            Webshop aliexpress = new Webshop(new DHLExpress());
            aliexpress.Rendeles();

            Console.ReadLine();
        }
    }
}
