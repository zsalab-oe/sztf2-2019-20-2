﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Tipusatalakitas
{
    class Allat { }
    class Macska : Allat { }
    class Kutya : Allat { }

    class Program
    {
        static void Main(string[] args)
        {
            double pi = 3.14;
            int x = (int)pi; // x = 3

            Allat cirmos = new Macska();
            Allat amoba = new Allat();
            Macska lukrecia;
            Kutya bodri;

            //lukrecia = (Macska)amoba;
            lukrecia = (Macska)cirmos;

            Macska szerenke = cirmos as Macska;
            szerenke = amoba as Macska;

            if (amoba is Macska)
                szerenke = amoba as Macska;

            //A kasztolás kivételt dob, az as null-t ad vissza


            Console.ReadLine();
        }
    }
}
