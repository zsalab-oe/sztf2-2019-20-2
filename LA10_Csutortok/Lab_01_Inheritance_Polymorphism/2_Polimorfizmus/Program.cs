﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Polimorfizmus
{
    class Allat
    {
        public void Bemutatkozas()
        {
            Console.WriteLine("Szia, állat vagyok!");
        }

        public virtual void Leiras()
        {
            Console.WriteLine("Általában állatkodom.");
        }
    }

    class Madar : Allat
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, madár vagyok!");
        }

        public override void Leiras()
        {
            Console.WriteLine("Repülök.");
        }
    }

    class Emlos : Allat
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, emlős vagyok!");
        }

        public override void Leiras()
        {
            Console.WriteLine("Szülök.");
        }
    }

    class Kutya : Emlos
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, kutya vagyok!");
        }

        public override void Leiras()
        {
            Console.WriteLine("Ugatok.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // nem-virtuális metódusok használata

            Allat amoba = new Allat();
            amoba.Bemutatkozas();

            Kutya morzsa = new Kutya();
            morzsa.Bemutatkozas();

            Allat masikHivatkozas = morzsa;
            masikHivatkozas.Bemutatkozas();

            Console.WriteLine("=====================");

            // virtuális metódusok használata

            amoba.Leiras();
            morzsa.Leiras();
            masikHivatkozas.Leiras();



            Console.WriteLine("==================");

            Allat[] allatok = new Allat[]
            {
                morzsa,
                new Emlos(),
                new Madar(),
                amoba,
                new Emlos(),
                new Kutya()
            };

            // Polimorfizmus nem-virtuális metódussal? Így nincs polimorfizmus!
            // lefuttatva látható, hogy mindenkinél az Allat osztályban létrehozott metódus fut le

            for (int i = 0; i < allatok.Length; i++)
            {
                allatok[i].Bemutatkozas();
            }

            Console.WriteLine("==================");

            // Polimorfizmus virtuális metódussal
            // lefuttatva látható, hogy mindenkinél a hivatkozott OBJEKTUM típusának megfelelő osztály metódusa fut le

            for (int i = 0; i < allatok.Length; i++)
            {
                allatok[i].Leiras();
            }

            Console.ReadLine();
        }
    }
}
