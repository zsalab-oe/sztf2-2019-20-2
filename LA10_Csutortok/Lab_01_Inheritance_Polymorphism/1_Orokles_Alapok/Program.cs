﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Orokles_Alapok
{
    class Allat
    {
        // prop + Tab + Tab
        // string nev; + getter, setter property
        // propg + Tab + Tab
        // string nev; + getter, private setter property

        public int Eletkor { get; set; } 
        //public int Eletkor2 { get; private set; } 
        
        public void Fut()
        {
            //this.kedvencEledel
        }

        private int labszam;

        protected string kedvencEledel;
        
        // ctor + Tab + Tab
        public Allat(int labszam)
        {
            this.labszam = labszam;
        }
    }

    class Emlos : Allat
    {
        public bool KicsinyetEteti()
        {
            this.kedvencEledel = "asdsa";
            return true;
        }


        // kötelező meghívni az ősosztály (Allat) konstruktorát még a leszármazott (Emlos) konstruktorának lefutása előtt --> "base" kulcsszóval
        public Emlos(int labszam)
            : base(labszam)
        {
            
        }
        
    }

    class Kutya : Emlos
    {
        public void FarkatCsovalja() { }
        public void Ugat() { }

        private int farokHossz;
        
        public Kutya(int farokHossz, int labszam)
            : base(labszam)
        {
            this.farokHossz = farokHossz;
        }
    }

    class Pok : Allat
    {
        public Pok()
            : base(8)
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Allat elsoAllat = new Allat(100);
            //elsoAllat.
            Emlos masodikAllat = new Emlos(4);
            //masodikAllat.
            Kutya harmadikAllat = new Kutya(15, 4);
            //harmadikAllat.
            Pok negyedikAllat = new Pok();
            //negyedikAllat.


            // *****************************************
            // Típuskompatibilitás
            // Minden T típusú objektumra hivatkozhatunk T típusú vagy T bármelyik őse típusú referenciával
            // egyenlőségjel bal oldalán: referencia
            // egyenlőségjel jobb oldalán: objektum

            Allat valamilyenAllat = new Kutya(15, 4);
            Allat megEgyAllat = new Pok();
            //Pok pokica = new Allat();
            //valamilyenAllat.

            Allat[] allatok = new Allat[]
            {
                valamilyenAllat,
                negyedikAllat,
                masodikAllat,
                new Allat(100),
                new Emlos(2)
            };

            for (int i = 0; i < allatok.Length; i++)
            {
                //allatok[i].
            }

            // az ős referenciáján keresztül a leszármazottakban felvett új mezők és metódusok nem érhetők el
        }
    }
}
