﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    // ==============================================================================================================
    // 
    // Absztrakt ősosztály
    //
    // ==============================================================================================================

    abstract class BacktrackBase
    {
        int N;
        Dolgozo[,] R;
        int[] M;

        public BacktrackBase(int n, Dolgozo[,] r, int[] m)
        {
            N = n;
            R = r;
            M = m;
        }

        protected abstract bool Ft(int szint, Dolgozo dolgozo);
        protected abstract bool Fk(int szint, Dolgozo dolgozo, Dolgozo[] E);

        void Backtrack(int szint, Dolgozo[] E, ref bool van)
        {
            int i = -1;
            while (!van && i < M[szint] - 1)
            {
                i++;
                if (Ft(szint, R[szint, i]))
                {
                    if (Fk(szint, R[szint, i], E))
                    {
                        E[szint] = R[szint, i];
                        if (szint == N - 1)
                        {
                            van = true;
                        }
                        else
                        {
                            Backtrack(szint + 1, E, ref van);
                        }
                    }
                }
            }
        }

        public bool MegoldasKereses(Dolgozo[] E)
        {
            bool van = false;
            Backtrack(0, E, ref van);
            return van;
        }
    }
}
