﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            Bankszamla szamla = new Bankszamla("Józsi", 5000);

            Felvetel(szamla, 1000);

            Felvetel(szamla, 6000);

            szamla.Aktiv = false;
            Felvetel(szamla, 200);

            Felvetel(szamla, 96000);

            Console.ReadLine();
        }

        static void Felvetel(Bankszamla szamla, int osszeg)
        {
            try
            {
                Console.WriteLine($">> Felvétel ({osszeg}Ft)");
                szamla.PenzFelvetel(osszeg);
                Console.WriteLine(">> A pénzfelvétel sikerült.");
            }
            catch (PenzFelvetelException e)
            {
                Console.WriteLine($"{DateTime.Now} - {e.Szamla.Tulajdonos} számláján nincs elegendő összeg, hiányzik: {e.HianyzoOsszeg}.");
            }
            catch (BankszamlaException e)
            {
                Console.WriteLine($"{DateTime.Now} - {e.Szamla.Tulajdonos} számlája jelenleg le van tiltva.");
            }
        }
    }
}
