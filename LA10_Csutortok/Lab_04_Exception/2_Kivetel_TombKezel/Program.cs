﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Kivetel_TombKezel
{
    class Program
    {
        static void Main(string[] args)
        {
            // - a program helyes (elvárt) működése és az esetleges hibák kezelése jól elkülönül egymástól
            // - a TombKezeles() metódus nincs megtördelve ellenőrzésekkel, hibakezelésekkel, csak az elvárt működést leíró kódot tartalmazza tömören, átláthatóan

            try
            {
                TombKezeles();
            }
            catch (FormatException)
            {
                Console.WriteLine("HIBA - nem megfelelő formátum. (Számjegyeket írj!)");
            }
            catch (OverflowException)
            {
                Console.WriteLine("HIBA - túl nagy szám.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("HIBA - kiindexeltél a tömbből.");
            }


            Console.ReadLine();
        }



        static void TombKezeles()
        {
            Console.WriteLine("Add meg a tömb méretét!");
            int[] tomb = new int[int.Parse(Console.ReadLine())];
            Console.WriteLine("Add meg a tömb elemeit!");
            for (int i = 0; i < tomb.Length; i++)
            {
                Console.Write((i + 1) + ".: ");
                tomb[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Hanyadik elemre vagy kíváncsi?");
            Console.WriteLine(tomb[int.Parse(Console.ReadLine()) - 1]);
        }






        static void SzamKezeles() // TryParse() példa
        {
            Console.Write("TryParse() példa - Adj meg egy számot: ");
            int szam;
            if (int.TryParse(Console.ReadLine(), out szam))
                Console.WriteLine("Szám = " + szam);
            else
                Console.WriteLine("Nem sikerült :(");

            /* int.TryParse():
             * - megpróbálja átalakítani a string-et számmá
             * - a szám címszerinti paraméterátadással kap értéket
             * - ha sikerült átalakítani (a string egy számot tartalmazott), igazat ad vissza, és közben a szám értéke a string-ben lévő érték lesz
             * - ha nem sikerült, akkor NEM száll el a program FormatException hibával (ellentétben az int.Parse() függvénnyel), csak hamisat ad vissza, és közben a szám értéke 0 lesz */
        }
    }
}
