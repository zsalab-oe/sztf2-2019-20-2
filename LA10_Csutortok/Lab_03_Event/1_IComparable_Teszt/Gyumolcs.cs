﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_IComparable_Teszt
{
    class Gyumolcs : IComparable
    {
        public string Nev { get; }
        public int Ar { get; }

        // Ctrl + .
        public Gyumolcs(string nev, int ar)
        {
            Nev = nev;
            Ar = ar;
        }

        public override string ToString()
        {
            return $"{Nev}\t{Ar}";
        }

        // IComparable interfész miatt kell
        public int CompareTo(object obj)
        {
            // CompareTo-n belül ÉN tudom megmondani, hogy MI ALAPJÁN legyen az összehasonlítás!

            // visszatérési érték
            //   - kisebb mint 0: (a Gyumolcs objektumokat rendezve) a 'this' példány megelőzi a paraméterként átadottat a sorrendben
            //   - 0: a példány és a paraméterként átadott azonos helyen szerepelnek a sorrendben
            //   - nagyobb mint 0: a paraméterként átadott előzi meg a példányt a sorrendben

            Gyumolcs masikGyumi = obj as Gyumolcs;

            if (this.Ar < masikGyumi.Ar)
                return -1;
            else if (this.Ar > masikGyumi.Ar)
                return 1;
            else
                return 0;

            //return this.Ar - masikGyumi.Ar;
        }
    }
}
