﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Esemenykezeles_Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Telefon t1 = new Telefon("1234567");
            Telefon t2 = new Telefon("4444444");

            // eseménykezelő objektum példányosítása
            HivasNaplo naplo = new HivasNaplo();

            // eseménykezelő objektum "regisztrálása”
            t1.FigyeloRegisztral(naplo);
            t2.FigyeloRegisztral(naplo);

            // teszt
            t1.EgyenlegFeltolt(50);
            t1.HivasKezdemenyemezes(t2);

            Console.WriteLine();
            t2.HivasKezdemenyemezes(t1);

            Console.ReadLine();
        }
    }
}
