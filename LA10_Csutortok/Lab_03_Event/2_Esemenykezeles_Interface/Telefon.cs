﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Esemenykezeles_Interface
{
    class Telefon
    {
        const int HIVASDIJ = 20;

        public string Telefonszam { get; private set; }
        public int Egyenleg { get; private set; }

        IHivasFigyelo hivasFigyelo; // referencia az eseménykezelő objektumra, ezen keresztül tudjuk hívni az eseménykezelő metódusokat

        public Telefon(string telefonszam)
        {
            Telefonszam = telefonszam;
            this.Egyenleg = 0;
        }

        public void EgyenlegFeltolt(int osszeg)
        {
            Egyenleg += osszeg;
        }

        public void FigyeloRegisztral(IHivasFigyelo hivasFigyelo) // eseménykezelő objektum "regisztrálása”
        {
            this.hivasFigyelo = hivasFigyelo;
        }

        public void HivasKezdemenyemezes(Telefon cel)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.KimenoHivasTortent(this, cel.Telefonszam); // jelzés az "eseményről" (történt valami)

            if (Egyenleg >= HIVASDIJ)
            {
                cel.HivasFogadas(this);
                Egyenleg -= HIVASDIJ;
            }
        }

        void HivasFogadas(Telefon forras)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.BejovoHivasTortent(this, forras.Telefonszam); // jelzés az "eseményről" (történt valami)
        }
    }
}
