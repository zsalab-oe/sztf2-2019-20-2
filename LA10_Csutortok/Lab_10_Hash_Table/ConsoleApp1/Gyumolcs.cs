﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Gyumolcs
    {
        public string Nev { get; }
        public Gyumolcs(string nev)
        {
            Nev = nev;
        }

        public override int GetHashCode()
        {
            return Nev.GetHashCode();
        }

        public override string ToString()
        {
            return Nev;
        }
    }
}
