﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Lancolt_Lista_Alapok
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista lista = new LancoltLista();

            lista.BeszurasElejere("Aladár");
            lista.BeszurasElejere("Béla");
            lista.BeszurasVegere("Cecil");
            lista.BeszurasElejere("Dénes");
            lista.BeszurasVegere("Elemér");

            try
            {
                lista.Torles("Dénes");
                lista.Torles("XY");
            }
            catch (NincsIlyenElemException e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Bejárás:");

            lista.Bejaras(Kiir);


            Console.ReadLine();
        }

        static void Kiir(string szoveg)
        {
            Console.WriteLine(szoveg);
        }
    }
}
