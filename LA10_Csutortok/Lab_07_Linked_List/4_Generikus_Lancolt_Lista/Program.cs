﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Generikus_Lancolt_Lista
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<string> nevek = new LancoltLista<string>();
            nevek.BeszurasElejere("Dénes");
            nevek.BeszurasElejere("Cecil");
            nevek.BeszurasElejere("Béla");
            nevek.BeszurasElejere("Aladár");

            nevek.Bejaras(Kiir);

            Console.WriteLine();

            LancoltLista<Szemely> szemelyek = new LancoltLista<Szemely>();
            szemelyek.BeszurasElejere(new Szemely("Aladár", 22));
            szemelyek.BeszurasElejere(new Szemely("Béla", 45));
            szemelyek.BeszurasElejere(new Szemely("Cecil", 21));
            szemelyek.BeszurasElejere(new Szemely("Dénes", 3));

            szemelyek.Bejaras(Kiir);

            Console.ReadLine();
        }

        static void Kiir<T>(T obj)
        {
            Console.WriteLine(obj.ToString());
        }
    }
}
