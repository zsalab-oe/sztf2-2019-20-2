﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Generikus_Lancolt_Lista
{
    delegate void Muvelet<T>(T tartalom);

    class LancoltLista<T>
    {
        class ListaElem
        {
            public T tartalom;
            public ListaElem kovetkezo;
        }

        ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }

        public void Bejaras(Muvelet<T> metodus)
        {
            Muvelet<T> _metodus = metodus;

            ListaElem p = fej;
            while (p != null)
            {
                _metodus?.Invoke(p.tartalom);
                p = p.kovetkezo;
            }
        }
    }
}
