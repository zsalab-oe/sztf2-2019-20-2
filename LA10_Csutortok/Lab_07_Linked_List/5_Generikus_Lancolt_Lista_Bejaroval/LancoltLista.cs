﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Generikus_Lancolt_Lista_Bejaroval
{
    class LancoltLista<T> : IEnumerable<T>
    {
        class ListaElem
        {
            public T tartalom;
            public ListaElem kovetkezo;
        }


        /* a "bejárást elvégző" objektum osztálya */
        class ListaBejaro : IEnumerator<T>
        {
            ListaElem elso;         // elmentünk egy hivatkozást a lista első elemére

            ListaElem aktualis;     // ezzel mutatunk mindig a lista aktuális elemére a bejárás során

            bool bejarasElindult;   // a bejarasElindult mező igazából csak azért kell, hogy a MoveNext() metódus egészen addig hamisat adjon vissza, amíg meg nem hívjuk a Reset()-et

            public ListaBejaro(ListaElem elso)
            {
                this.elso = elso;
                bejarasElindult = false;
            }

            // az aktuális elem tartalmi részét adja vissza (a foreach ciklusváltozója ennek az értékét veszi fel)
            // megjegyzés: foreach-en belül ezért nem módosíthatók egy gyűjtemény elemei, mert a tulajdonság csak olvasható
            public T Current { get { return aktualis.tartalom; } }

            object IEnumerator.Current { get { return this.Current; } }

            // erőforrások felszabadítására szolgál
            // a foreach a bejárás végeztével automatikusan meghívja
            // esetünkben hagyhatnánk üresen is (foreach-nél a bejárás végeztével úgyis megszűnik a ListaBejaro objektum)
            public void Dispose()
            {
                elso = null;
                aktualis = null;
            }

            // lépteti az "aktualis" hivatkozást a következő listaelemre, és igazat vagy hamisat ad vissza attól függően, hogy listaelemre lépett-e vagy nem (lista vége, vagy üres volt a lista)
            public bool MoveNext()
            {
                if (!bejarasElindult)                   // első hívás
                {
                    aktualis = elso;                    // rálép a lista első elemére...
                    bejarasElindult = true;
                }
                else if (aktualis != null)              // n. hívás (n > 1)
                {
                    aktualis = aktualis.kovetkezo;      // átlép a lista következő elemére...
                }
                return aktualis != null;                // ...igaz ha van ilyen, hamis ha nincs
            }

            // alaphelyzetbe állítja a bejárót
            public void Reset()
            {
                aktualis = null;
                bejarasElindult = false;
            }
        }


        private ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }


        // létrehoz és visszaad egy IEnumerator<T> interfészt megvalósító objektumot
        public IEnumerator<T> GetEnumerator()
        {
            return new ListaBejaro(fej);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
